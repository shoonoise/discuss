import json
import time
from datetime import datetime

from django.test import TestCase
from django.test import Client
from app.models import Entity, EntityType


class CommentTests(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.client = Client()

        entity_type = EntityType.objects.create(name='url')
        Entity.objects.create(value='localhost', entity_type=entity_type)

    @classmethod
    def tearDownClass(cls):
        pass

    def check_resp(self, response):
        r = response.json()

        if r['status'] == "ok":
            return r['body']
        else:
            raise Exception("Response not ok: %s" % r['body'])

    def create_comment(self, data):
        response = self.client.post('/api/comments/url/localhost/', data=json.dumps(data),
                                    content_type="application/json")
        return self.check_resp(response)

    def get_comment(self, comment_id):
        response = self.client.get('/api/comments/url/localhost/%s' % comment_id)
        return self.check_resp(response)

    def get_comments(self):
        response = self.client.get('/api/comments/url/localhost/')
        return self.check_resp(response)

    def update_comment(self, comment_id, text):
        response = self.client.patch('/api/comments/url/localhost/%d' % comment_id, data=json.dumps({"text": text}),
                                     content_type="application/json")
        return self.check_resp(response)

    def generate_comment_tree(self):
        first_lvl_comment_1 = {
            "user": "first_commentator",
            "text": "FIRST",
            "in_reply_to": None
        }
        first_lvl_comment_2 = {
            "user": "second_commentator",
            "text": "SECOND",
            "in_reply_to": None
        }
        first_lvl_comment_3 = {
            "user": "first_commentator",
            "text": "THIRD",
            "in_reply_to": None
        }
        first_lvl_comments = [comment for comment in
                              map(self.create_comment,
                                  [first_lvl_comment_1, first_lvl_comment_2, first_lvl_comment_3])]
        second_lvl_comment_1 = {
            "user": "second_level_commentator_1",
            "text": "Reply to first comment",
            "in_reply_to": first_lvl_comments[0]['id']
        }
        reply_to_first_lvl_comment_1 = self.create_comment(second_lvl_comment_1)
        third_lvl_comment_1 = {
            "user": "third_level_commentator_1",
            "text": "Reply to reply to first comment",
            "in_reply_to": reply_to_first_lvl_comment_1['id']
        }
        self.create_comment(third_lvl_comment_1)
        second_lvl_comment_2 = {
            "user": "second_level_commentator_3",
            "text": "Reply to second comment",
            "in_reply_to": first_lvl_comments[1]['id']
        }
        self.create_comment(second_lvl_comment_2)
        self.create_comment(second_lvl_comment_2)

        return first_lvl_comments, first_lvl_comment_2, second_lvl_comment_1, second_lvl_comment_2, third_lvl_comment_1

    def test_add_new_comment(self):
        data = {
            "user": "test_user",
            "text": "text comment",
            "in_reply_to": None
        }
        awaited_comment = self.create_comment(data)
        actual_comment = self.get_comment(awaited_comment['id'])

        self.assertDictEqual(awaited_comment, actual_comment)

    def test_add_reply(self):
        original_comment = {
            "user": "test_user_1",
            "text": "text comment",
            "in_reply_to": None
        }

        parent_comment = self.create_comment(original_comment)

        reply_comment = {
            "user": "test_user_2",
            "text": "text child comment",
            "in_reply_to": parent_comment['id']
        }

        actual_reply_comment = self.create_comment(reply_comment)

        updated_parent_comment = self.get_comment(parent_comment['id'])
        replies = updated_parent_comment['replies']

        self.assertDictEqual(actual_reply_comment, replies[0], updated_parent_comment)

    def test_update_comment(self):
        original_comment = {
            "user": "test_user_1",
            "text": "text comment",
            "in_reply_to": None
        }

        new_text = "updated comment"

        comment = self.create_comment(original_comment)

        # Needs to detect time change update
        time.sleep(1)  # TODO: find a way to detect time deltas less than second

        self.update_comment(comment['id'], new_text)
        updated_comment = self.get_comment(comment['id'])

        self.assertEqual(updated_comment['text'], new_text)
        self.assertGreater(datetime.fromtimestamp(updated_comment['updated']),
                           datetime.fromtimestamp(comment['created']))

    def test_get_comments_tree(self):

        (first_lvl_comments, first_lvl_comment_2, second_lvl_comment_1,
         second_lvl_comment_2, third_lvl_comment_1) = self.generate_comment_tree()

        comments = self.get_comments()

        self.assertEqual(comments[0]['replies'][0]['text'], second_lvl_comment_1['text'], comments)
        self.assertEqual(comments[0]['replies'][0]['user'], second_lvl_comment_1['user'], comments)
        self.assertEqual(comments[0]['replies'][0]['replies'][0]['text'], third_lvl_comment_1['text'], comments)
        self.assertEqual(comments[0]['replies'][0]['replies'][0]['user'], third_lvl_comment_1['user'], comments)
        self.assertEqual(comments[1]['text'], first_lvl_comment_2['text'], comments)
        self.assertEqual(comments[1]['user'], first_lvl_comment_2['user'], comments)
        self.assertEqual(comments[1]['replies'][0]['text'], second_lvl_comment_2['text'], comments)
        self.assertEqual(comments[1]['replies'][1]['text'], second_lvl_comment_2['text'], comments)

    def test_get_comments_tree_pagination(self):
        (first_lvl_comments, first_lvl_comment_2, second_lvl_comment_1,
         second_lvl_comment_2, third_lvl_comment_1) = self.generate_comment_tree()

        response = self.client.get('/api/comments/url/localhost/?per_page=1')
        comments = self.check_resp(response)

        self.assertEqual(len(comments), 1)
        self.assertEqual(comments[0]['replies'][0]['text'], second_lvl_comment_1['text'], comments)

        response = self.client.get('/api/comments/url/localhost/%s' % response.json()['next'])
        comments = self.check_resp(response)

        self.assertEqual(len(comments), 1)
        self.assertEqual(comments[0]['replies'][0]['text'], second_lvl_comment_2['text'], comments)

        response = self.client.get('/api/comments/url/localhost/%s' % response.json()['next'])
        comments = self.check_resp(response)

        self.assertEqual(len(comments), 1)
        self.assertEqual(comments[0]['text'], first_lvl_comments[2]['text'], comments)

        response = self.client.get('/api/comments/url/localhost/?per_page=2')
        comments = self.check_resp(response)

        self.assertEqual(len(comments), 2)
        self.assertEqual(comments[0]['replies'][0]['text'], second_lvl_comment_1['text'], comments)
        self.assertEqual(comments[1]['replies'][0]['text'], second_lvl_comment_2['text'], comments)

    def test_get_first_level_comments(self):
        (first_lvl_comments, first_lvl_comment_2, second_lvl_comment_1,
         second_lvl_comment_2, third_lvl_comment_1) = self.generate_comment_tree()

        response = self.client.get('/api/comments/url/localhost/?first_level=1')
        comments = self.check_resp(response)

        self.assertEqual(len(comments), 3)
        self.assertEqual(comments[0]['text'], first_lvl_comments[0]['text'], comments)
        self.assertEqual(comments[0]['replies'], [], comments)

    def test_get_history(self):
        comment_1 = {
                "user": "test_user_1",
                "text": "text comment",
                "in_reply_to": None
            }

        self.create_comment(comment_1)
        original_comment = self.create_comment(comment_1)

        comment_2 = {
                "user": "test_user_1",
                "text": "text comment",
                "in_reply_to": original_comment['id']
            }

        self.create_comment(comment_2)

        response = self.check_resp(self.client.get('/api/history/users/test_user_1'))

        self.assertEqual(len(response), 3)
        self.assertEqual(response[0]['text'], comment_1['text'])
        self.assertEqual(response[1]['text'], comment_1['text'])
        self.assertEqual(response[2]['text'], comment_2['text'])
