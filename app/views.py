import json
from datetime import datetime

from django.http import HttpResponse
from django.views.decorators.http import require_http_methods
from app.models import Comment, EntityType, Entity, UserHistoryUploads, EntityHistoryUploads
from django.core.exceptions import ObjectDoesNotExist
from discuss.celery import celery_app
from .tasks import generate_file
from .utils import dt_to_timestamp


class WrongRequest(Exception):
    pass


def success_api_response(body, extra=None):
    reply = {'status': 'ok', 'body': body}
    if extra:
        reply.update(extra)
    return HttpResponse(json.dumps(reply), content_type="application/json")


def err_api_response(status, body):
    reply = {'status': 'err', 'body': str(body)}
    return HttpResponse(json.dumps(reply), content_type="application/json", status=status)


def get_entity(entity_type_name, entity_value):
    if not (entity_type_name and entity_value):
        raise WrongRequest("You should set both of entity_type_name and entity_value")

    entity_type = EntityType.objects.get(name=entity_type_name)
    entity = Entity.objects.get(entity_type=entity_type, value=entity_value)

    return entity


def update_comment(comment_id, entity, body):
    try:
        comment = Comment.tree.filter(entity=entity).get(pk=comment_id)
    except ObjectDoesNotExist as e:
        return err_api_response(404, e)
    comment.text = body['text']
    comment.save()
    return success_api_response(comment.node_as_json())


def delete_comment(comment_id, entity):
    try:
        comment = Comment.tree.filter(entity=entity).get(pk=comment_id)
    except ObjectDoesNotExist as e:
        return err_api_response(404, e)
    if len(comment.get_children()) > 0:
        return err_api_response(400, "You can't delete comment with children.")
    comment.delete()
    return success_api_response('comment %d was deleted' % comment_id)


def get_comment(comment_id, entity):
    try:
        comment = Comment.tree.filter(entity=entity).get(pk=comment_id)
    except ObjectDoesNotExist as e:
        return err_api_response(404, e)
    return success_api_response(comment.as_tree())


def get_comments(entity, first_level, page, per_page):
    roots = Comment.tree.root_nodes().filter(entity=entity)
    origina_roots_len = len(roots)
    extra = {}
    if per_page > 0:
        start_pos = page * per_page
        roots = roots[start_pos:start_pos + per_page]
        extra = {'page': page, 'per_page': per_page}
        if origina_roots_len > start_pos + per_page:
            extra['next'] = '?page=%d&per_page=%d' % (page + 1, per_page)
        if page > 0:
            extra['previous'] = '?page=%d&per_page=%d' % (page - 1, per_page)
    if first_level:
        comments = [comment.node_as_json() for comment in roots]
    else:
        comments = [comment.as_tree() for comment in roots]
    return success_api_response(comments, extra)


def create_file_gen_task(fltr, request, target):
    file_format = request.GET.get('file_format')
    start_from = request.GET.get('start_from')
    end_to = request.GET.get('end_to')
    if start_from:
        fltr['created__gte'] = start_from
    if end_to:
        fltr['created__lt'] = end_to
    comments = [comment.node_as_json() for comment in Comment.tree.filter(**fltr)]
    if file_format is None:
        return success_api_response(comments)
    else:
        task = generate_file.delay(comments, file_format, '%s(%s).%s' % (target, datetime.now(), file_format))
        return success_api_response({'task_id': task.id})


def get_history_file_link(task_id, target_type, target):
    res = celery_app.AsyncResult(task_id)
    if not res.ready():
        return err_api_response(404, "Not ready yet. Status: %s" % res.state)
    if res.state == 'FAILURE':
        return err_api_response(500, "File creating failed: %s" % res.info)
    file_path = res.get()
    download_url = "static/history/%s" % file_path
    if target_type == "user":
        UserHistoryUploads.objects.create(user=target, url=download_url)
    elif target_type == "entity":
        EntityHistoryUploads.objects.create(entity=target, url=download_url)
    return success_api_response({'file_link': download_url})


def with_entity(fn):
    def wrap(request, entity_type, entity_value, *args, **kwargs):
        try:
            entity = get_entity(entity_type, entity_value)
        except ObjectDoesNotExist as e:
            return err_api_response(404, e)
        except WrongRequest as e:
            return err_api_response(400, e)
        return fn(request, entity, *args, **kwargs)

    return wrap


@require_http_methods(["GET", "PATCH", "DELETE"])
@with_entity
def comment_handler(request, entity, comment_id):
    if request.method == "GET":
        return get_comment(comment_id, entity)
    elif request.method == "PATCH":
        body = json.loads(request.body.decode('utf-8'))
        return update_comment(comment_id, entity, body)
    else:
        return delete_comment(comment_id, entity)


@require_http_methods(["POST", "GET"])
@with_entity
def comments_handler(request, entity):
    if request.method == "POST":
        body = json.loads(request.body.decode('utf-8'))
        is_reply_to = body.get('in_reply_to')
        ancestor = None
        if is_reply_to is not None:
            try:
                ancestor = Comment.tree.get(pk=is_reply_to)
            except ObjectDoesNotExist as e:
                return err_api_response(404, e)
        user = body.get('user')
        text = body.get('text')
        comment = Comment.objects.create(user=user, text=text, entity=entity, parent=ancestor)
        return success_api_response(comment.node_as_json())

    else:
        first_level = request.GET.get('first_level', False)
        try:
            page = int(request.GET.get('page', 0))
            per_page = int(request.GET.get('per_page', 0))
        except TypeError as e:
            return err_api_response(400, "Wrong parameters value: %s" % e)

        return get_comments(entity, first_level, page, per_page)


@require_http_methods(["GET"])
def user_history_handler(request, user):
    fltr = {'user': user}
    return create_file_gen_task(fltr, request, user)


@require_http_methods(["GET"])
@with_entity
def entity_history_handler(request, entity):
    fltr = {'entity': entity}
    return create_file_gen_task(fltr, request, entity)


@require_http_methods(["GET"])
def user_history_file_handler(request, user, task_id):
    return get_history_file_link(task_id, 'user', user)


@require_http_methods(["GET"])
@with_entity
def entity_history_file_handler(request, entity, task_id):
    return get_history_file_link(task_id, 'entity', entity)


@require_http_methods(["GET"])
def user_history_uploads(request, user):
    records = UserHistoryUploads.objects.filter(user=user)
    return success_api_response([{"url": record.url, "created": dt_to_timestamp(record.created)} for record in records])


@require_http_methods(["GET"])
@with_entity
def entity_history_uploads(request, entity):
    records = EntityHistoryUploads.objects.filter(entity=entity)
    return success_api_response([{"url": record.url, "created": dt_to_timestamp(record.created)} for record in records])


@require_http_methods(["POST"])
def entities_handler(request):
    value = request.POST.get('value')
    entity_type = request.POST.get('type')

    if not (value and entity_type):
        return err_api_response(400, "You should set both of entity fields: `type` and `value`.")
    try:
        EntityType.objects.get(name=entity_type)
    except ObjectDoesNotExist:
        entity_type = EntityType.objects.create(name=entity_type)
    else:
        entity_type = EntityType.objects.create(name=entity_type)
    Entity.objects.create(value=value, entity_type=entity_type)
    return success_api_response("created")
