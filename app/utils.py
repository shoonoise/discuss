import time
from datetime import datetime


def dt_to_timestamp(dt: datetime) -> float:
    """
    make timestamp from datetime object
    :param dt: datetime object
    """
    return time.mktime(dt.timetuple())
