from django.db import models
from mptt.models import MPTTModel, TreeForeignKey, TreeManager

from app.utils import dt_to_timestamp


class EntityType(models.Model):
    name = models.TextField(max_length=50)

    def __str__(self):
        return self.name


class Entity(models.Model):
    value = models.TextField(max_length=50)
    entity_type = models.ForeignKey(EntityType)

    def __str__(self):
        return "{}:{}".format(self.entity_type.name, self.value)


class Comment(MPTTModel):
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True)
    user = models.TextField(max_length=50, db_index=True)
    create = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    text = models.TextField(max_length=140)

    entity = models.ForeignKey(Entity, db_index=True)

    tree = TreeManager()

    def __str__(self):
        return "{}\t{}\t{}:{}".format(self.id, self.create, self.user, self.text)

    def as_tree(self):
        """
        return comment and all replies to comment
        """
        return {'id': self.id, 'user': self.user, 'text': self.text,
                'created': dt_to_timestamp(self.create), 'updated': dt_to_timestamp(self.modified),
                'replies': [x.as_tree() for x in self.get_children()]}  # TODO: generator?

    def node_as_json(self):
        """
        return comment object as a dict
        replies list will be empty each time even the comment has the replies
        """
        return {'id': self.id, 'user': self.user, 'text': self.text,
                'created': dt_to_timestamp(self.create),
                'updated': dt_to_timestamp(self.modified), 'replies': []}


class UserHistoryUploads(models.Model):
    user = models.TextField(max_length=50)
    url = models.TextField(max_length=50)
    created = models.DateTimeField(auto_now_add=True)


class EntityHistoryUploads(models.Model):
    entity = models.ForeignKey(Entity)
    url = models.TextField(max_length=50)
    created = models.DateTimeField(auto_now_add=True)
