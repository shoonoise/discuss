import json
import os
import dicttoxml

from celery import shared_task


@shared_task
def generate_file(data, file_format, file_name):
    curr_dir = os.path.dirname(os.path.abspath(__file__))
    full_path = '%s/static/history/%s' % (curr_dir, file_name)

    if file_format.lower() == 'json':
        with open(full_path, 'w+') as f:
            json.dump(data, f)
    elif file_format.lower() == 'xml':
        with open(full_path, 'w+') as f:
            f.write(dicttoxml.dicttoxml(data))
    else:
        raise NotImplementedError("Not supported file format %s" % file_format)
    return file_name
