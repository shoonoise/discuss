#!/usr/bin/env bash

python3 manage.py migrate app &&\
python3 manage.py test &&\
python3 manage.py loaddata fixture.json &&\
celery -B -A discuss worker --loglevel=debug&\
python3 manage.py runserver 0.0.0.0:80
