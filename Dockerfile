FROM ubuntu:trusty
MAINTAINER Alexander Kushnarev <avkushnarev@gmail.com>

RUN apt-get update
RUN apt-get install -y curl python3 libpq-dev python3-dev

RUN curl -OL https://bootstrap.pypa.io/get-pip.py
RUN python3 get-pip.py

ADD . .

RUN python3 -m pip install -r requirements.txt

EXPOSE 80

CMD ./run.sh
