import os

from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'discuss.settings')

from django.conf import settings

celery_app = Celery('discuss')

celery_app.config_from_object('django.conf:settings')
celery_app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
