from django.conf.urls import url
from django.contrib import admin
from app import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/comments/(?P<entity_type>\w+)/(?P<entity_value>\w+)/(?P<comment_id>\d+)$', views.comment_handler),
    url(r'^api/comments/(?P<entity_type>\w+)/(?P<entity_value>\w+)/', views.comments_handler),
    url(r'^api/history/entities/(?P<entity_type>\w+)/(?P<entity_value>\w+)', views.entity_history_handler),
    url(r'^api/files/entities/(?P<entity_type>\w+)/(?P<entity_value>\w+)/(?P<task_id>\S+)',
        views.entity_history_file_handler),
    url(r'^api/files/entities/(?P<entity_type>\w+)/(?P<entity_value>\w+)/', views.entity_history_uploads),
    url(r'^api/history/users/(?P<user>\w+)', views.user_history_handler),
    url(r'^api/files/users/(?P<user>\w+)/(?P<task_id>\S+)', views.user_history_file_handler),
    url(r'^api/files/users/(?P<user>\w+)/', views.user_history_uploads),
    url(r'^api/entities/', views.entities_handler),
]
